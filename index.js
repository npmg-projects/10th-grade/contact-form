let full_name = document.getElementById('name');
let subject = document.getElementById('subject');
let phone = document.getElementById('phone');
let email = document.getElementById('email');
let message = document.getElementById('message');
let error = document.getElementById('error_message');
let regex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

function validate() {
  if (full_name.value.length < 3) {
    error.innerHTML = "Invalid name";
  }
  if (subject.value.length < 10) {
    error.innerHTML = "Invalid subject"
  }
  if (phone.value.length != 10) {
    error.innerHTML = "Invalid phone number";
  }
  if(message.value.length < 10) {
    error.innerHTML = "Invalid message";
  }
  if (!regex.test(email.value)) {
    error.innerHTML = "Invalid email";
  }
}
